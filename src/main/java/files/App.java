package files;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiPredicate;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import static files.LambdaExceptionUtils.rethrowFunction;

public class App {
    static Map<String, Propety> propety;
    static {
        propety = new Gson().fromJson("{\"000\":{\"id\":0,\"code\":\"a\"}, \"001\":{\"id\":1,\"code\":\"b\"}}", new TypeToken<HashMap<String, Propety>>(){}.getType());
    }

    public static void print(final Object o) {
        System.out.println("\t\tprint => " + o);
    }

    public static void walkFileTree(final Path start) throws IOException {
        Files.walkFileTree(start, new FileVisitor<Path>() {
            @Override
            public FileVisitResult preVisitDirectory(final Path dir, final BasicFileAttributes attrs)
                    throws IOException {
                System.out.println("\t" + dir);
                if (dir.equals(start) || propety.containsKey(dir.getFileName().toString())) {
                    return FileVisitResult.CONTINUE;
                }
                return FileVisitResult.SKIP_SUBTREE;
            }

            @Override
            public FileVisitResult visitFile(final Path file, final BasicFileAttributes attrs) throws IOException {
                System.out.println("\t" + file);
                if (file.getFileName().toString().startsWith(file.getParent().getFileName().toString())
                    && file.getFileName().toString().endsWith(".txt")) {
                    print(file);
                }
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult visitFileFailed(final Path file, final IOException exc) throws IOException {
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult postVisitDirectory(final Path dir, final IOException exc) throws IOException {
                return FileVisitResult.CONTINUE;
            }
        });
    }

    public static void find(final Path start) throws IOException {
        final BiPredicate<Path, BasicFileAttributes> matcher = (file, attr) -> {
            System.out.println("\t" + file);
            
            return attr.isRegularFile()
                    && propety.containsKey(file.getParent().getFileName().toString())
                    && file.getFileName().toString().startsWith(file.getParent().getFileName().toString())
                    && file.getFileName().toString().endsWith(".txt");
        };

        var stream = Files.find(start, 2, matcher);
        try(stream) {
            stream.forEach(App::print);
        }
    }

    public static void list(final Path start) throws IOException {
        var stream = Files.list(start);
        try(stream) {
            var stream2 = stream.filter(Files::isDirectory)
                .filter(p -> {
                    System.out.println("\t" + p);
                    return propety.containsKey(p.getFileName().toString());
                })
                .flatMap(rethrowFunction(p -> Files.list(p)));
            try(stream2) {
                stream2
                    .filter(Files::isRegularFile)
                    .filter(p -> p.getFileName().toString().startsWith(p.getParent().getFileName().toString()))
                    .filter(p -> {
                        System.out.println("\t" + p);
                        return p.getFileName().toString().endsWith(".txt");
                    })
                    .forEach(App::print);
            }
        }
    }

    public static void main(final String[] args) throws Exception {
        final var start = Paths.get("data");

        System.out.println("find");
        find(start);
        System.out.println();
        System.out.println("walkFileTree");
        walkFileTree(start);
        System.out.println();
        System.out.println("list");
        list(start);
    }
}