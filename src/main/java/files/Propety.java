package files;

public class Propety {
    public int id;
    public String code;

    public Propety(final int id, final String code) {
        this.id = id;
        this.code = code;
    }

    @Override
    public String toString() {
        return "Property [code=" + code + ", id=" + id + "]";
    }

}